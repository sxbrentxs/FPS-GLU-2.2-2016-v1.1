//InputControl.cs by Azuline Studios© All Rights Reserved
//Manages button and axis input to be read by the other scripts.
//
//Rewritten by Grafisch Lyceum student Brent Dijsselbloem.
using UnityEngine;
using System.Collections;

public class InputControl : MonoBehaviour
{
	FPSPlayer FPSPlayerComponent;

	//button states that are accessed by the other scripts
	bool fireHold;
	public bool GetFireHold { get { return fireHold; } }
	bool reloadPress;
	public bool GetReloadPress { get { return reloadPress; } }
	bool fireModePress;
	public bool GetFireModePress { get { return fireModePress; } }
	bool jumpHold;
	public bool GetJumpHold { get { return jumpHold; } }
	bool jumpPress;
	public bool GetJumpPress { get { return jumpPress; } }
	bool crouchHold;
	public bool GetCrouchHold { get { return crouchHold; } }
	bool proneHold;
	public bool GetProneHold { get { return proneHold; } }
	bool sprintHold;
	public bool GetSprintHold { get { return sprintHold; } }
	bool zoomHold;
	public bool GetZoomHold { get { return zoomHold; } }
	bool leanLeftHold;
	public bool GetLeanLeftHold { get { return leanLeftHold; } }
	bool leanRightHold;
	public bool GetLeanRightHold { get { return leanRightHold; } }
	bool useHold;
	public bool GetUseHold { get { return useHold; } }
	bool holsterPress;
	public bool GetHolsterPress { get { return holsterPress; } }
	bool dropPress;
	public bool GetDropPress { get { return dropPress; } }
	bool bulletTimePress;
	public bool GetBulletTimePress { get { return bulletTimePress; } }
	bool moveHold;
	public bool GetMoveHold { get { return moveHold; } }
	bool movePress;
	public bool GetMovePress { get { return movePress; } }
	bool throwHold;
	public bool GetThrowHold { get { return throwHold; } }
	bool helpPress;
	public bool GetHelpPress { get { return helpPress; } }
	bool pausePress;
	public bool GetPausePress { get { return pausePress; } }
	bool restartPress;
	public bool GetRestartPress { get { return restartPress; } }
	bool exitHold;
	public bool GetExitHold { get { return exitHold; } }
	bool selectNextPress;
	public bool GetSelectNextPress { get { return selectNextPress; } }
	bool selectPrevPress;
	public bool GetSelectPrevPress { get { return selectPrevPress; } }
	bool selectWeap1Press;
	public bool GetSelectWeapon1Press { get { return selectWeap1Press; } }
	bool selectWeap2Press;
	public bool GetSelectWeapon2Press { get { return selectWeap2Press; } }
	bool selectWeap3Press;
	public bool GetSelectWeapon3Press { get { return selectWeap3Press; } }
	bool selectWeap4Press;
	public bool GetSelectWeapon4Press { get { return selectWeap4Press; } }
	bool selectWeap5Press;
	public bool GetSelectWeapon5Press { get { return selectWeap5Press; } }
	bool selectWeap6Press;
	public bool GetSelectWeapon6Press { get { return selectWeap6Press; } }
	bool selectWeap7Press;
	public bool GetSelectWeapon7Press { get { return selectWeap7Press; } }
	bool selectWeap8Press;
	public bool GetSelectWeapon8Press { get { return selectWeap8Press; } }
	bool selectWeap9Press;
	public bool GetSelectWeapon9Press { get { return selectWeap9Press; } }
	bool selectWeap0Press;
	public bool GetSelectWeapon0Press { get { return selectWeap0Press; } }

	float mouseWheel;
	public float GetMouseWheel { get { return mouseWheel; } }

	bool leftHold;
	public bool GetLeftHold { get { return leftHold; } }
	bool rightHold;
	public bool GetRightHold { get { return rightHold; } }
	bool forwardHold;
	public bool GetForwardHold { get { return forwardHold; } }
	bool backHold;
	public bool GetBackHold { get { return backHold; } }
	float moveXButton;
	public float GetMoveXButton { get { return moveXButton; } }
	float moveYButton;
	public float GetMoveYButton { get { return moveYButton; } }

	//gamepad input axes
	float deadzone = 0.25f;
	public float GetDeadzone { get { return deadzone; } }
	Vector2 moveInput;
	Vector2 lookInput;

	//combined button and axis inputs for moving
	float moveX;
	public float GetMoveX { get { return moveX; } }
	float moveY;
	public float GetMoveY { get { return moveY; } }
	//combined button and axis inputs for looking
	float lookX;
	public float GetLookX { get { return lookX; } }
	float lookY;
	public float GetLookY { get { return lookY; } }

	//Xbox 360 dpad controls (button held)
	bool xboxDpadLeftHold;
	public bool GetXboxDpadLeftHold { get { return xboxDpadLeftHold; } }
	bool xboxDpadRightHold;
	public bool GetXboxDpadRightHold { get { return xboxDpadRightHold; } }
	bool xboxDpadUpHold;
	public bool GetXboxDpadUpHold { get { return xboxDpadUpHold; } }
	bool xboxDpadDownHold;
	public bool GetXboxDpadDownHold { get { return xboxDpadDownHold; } }

	//Xbox 360 dpad controls (button press)
	bool xboxDpadLeftPress;
	public bool GetXboxDpadLeftPress { get { return xboxDpadLeftPress; } }
	bool xboxDpadRightPress;
	public bool GetXboxDpadRightPress { get { return xboxDpadRightPress; } }
	bool xboxDpadUpPress;
	public bool GetXboxDpadUpPress { get { return xboxDpadUpPress; } }
	bool xboxDpadDownPress;
	public bool GetXboxDpadDownPress { get { return xboxDpadDownPress; } }

	bool xbdpLstate;
	bool xbdpRstate;
	bool xbdpUstate;
	bool xbdpDstate;

	void Start()
	{
		FPSPlayerComponent = GetComponent<FPSPlayer>();
	}

	void Update()
	{

		if (FPSPlayerComponent && !FPSPlayerComponent.restarting)
		{

			//player movement buttons
			leftHold = Input.GetButton("Left");
			rightHold = Input.GetButton("Right");
			forwardHold = Input.GetButton("Forward");
			backHold = Input.GetButton("Back");

			//cancel player movement if opposite buttons are held at the same time
			if (leftHold && !rightHold)
			{
				moveXButton = -1.0f;
			}
			else if (rightHold && !leftHold)
			{
				moveXButton = 1.0f;
			}
			else
			{
				moveXButton = 0.0f;
			}

			if (forwardHold && !backHold)
			{
				moveYButton = 1.0f;
			}
			else if (backHold && !forwardHold)
			{
				moveYButton = -1.0f;
			}
			else
			{
				moveYButton = 0.0f;
			}

			//scaled radial deadzone for joysticks for smooth player movement ramp from deadzone
			moveInput = new Vector2(Input.GetAxis("Joystick Move X"), Input.GetAxis("Joystick Move Y"));
			if (moveInput.magnitude < deadzone)
			{
				moveInput = Vector2.zero;
			}
			else
			{
				moveInput = moveInput.normalized * ((moveInput.magnitude - deadzone) / (1 - deadzone));
			}

			lookInput = new Vector2(Input.GetAxis("Joystick Look X"), Input.GetAxis("Joystick Look Y"));
			if (lookInput.magnitude < deadzone)
			{
				lookInput = Vector2.zero;
			}
			else
			{
				lookInput = lookInput.normalized * ((lookInput.magnitude - deadzone) / (1 - deadzone));
			}

			//combine button and axis input for player movement
			moveX = moveXButton + moveInput.x;
			moveY = moveYButton + moveInput.y;

			//combine mouse and axis input for player looking (accelerate axis input)
			lookX = Input.GetAxisRaw("Mouse X") + AccelerateInput(lookInput.x);
			lookY = Input.GetAxisRaw("Mouse Y") + AccelerateInput(lookInput.y);

			//manage zoom and fire inputs and determine if xbox 360 triggers have been pressed or held
			if (Input.GetAxisRaw("Xbox R Trigger") > 0.1f || Input.GetButton("Fire"))
			{
				fireHold = true;
			}
			else
			{
				fireHold = false;
			}

			if (Input.GetAxisRaw("Xbox L Trigger") > 0.1f || Input.GetButton("Zoom"))
			{
				zoomHold = true;
			}
			else
			{
				zoomHold = false;
			}

			//determine if the Xbox 360 dpad buttons have been pressed or held
			if (Input.GetAxis("Xbox Dpad X") > 0.0f)
			{
				xboxDpadRightHold = true;
				xboxDpadLeftHold = false;
				xbdpLstate = false;
				if (!xbdpRstate)
				{
					xboxDpadRightPress = true;
					xbdpRstate = true;
				}
				else
				{
					xboxDpadRightPress = false;
				}
			}
			else if (Input.GetAxis("Xbox Dpad X") < 0.0f)
			{
				xboxDpadRightHold = false;
				xboxDpadLeftHold = true;
				xbdpRstate = false;
				if (!xbdpLstate)
				{
					xboxDpadLeftPress = true;
					xbdpLstate = true;
				}
				else
				{
					xboxDpadLeftPress = false;
				}
			}
			else
			{
				xboxDpadRightHold = false;
				xboxDpadLeftHold = false;
				xboxDpadRightPress = false;
				xboxDpadLeftPress = false;
				xbdpLstate = false;
				xbdpRstate = false;
			}

			if (Input.GetAxis("Xbox Dpad Y") > 0.0f)
			{
				xboxDpadUpHold = true;
				xboxDpadDownHold = false;
				xbdpDstate = false;
				if (!xbdpUstate)
				{
					xboxDpadUpPress = true;
					xbdpUstate = true;
				}
				else
				{
					xboxDpadUpPress = false;
				}
			}
			else if (Input.GetAxis("Xbox Dpad Y") < 0.0f)
			{
				xboxDpadUpHold = false;
				xboxDpadDownHold = true;
				xbdpUstate = false;
				if (!xbdpDstate)
				{
					xboxDpadDownPress = true;
					xbdpDstate = true;
				}
				else
				{
					xboxDpadDownPress = false;
				}
			}
			else
			{
				xboxDpadUpHold = false;
				xboxDpadDownHold = false;
				xboxDpadUpPress = false;
				xboxDpadDownPress = false;
				xbdpUstate = false;
				xbdpDstate = false;
			}

			//read button input and set the button state vars for use by the other scripts
			mouseWheel = Input.GetAxis("Mouse Scroll Wheel");

			reloadPress = Input.GetButtonDown("Reload");
			fireModePress = Input.GetButtonDown("Fire Mode");
			jumpHold = Input.GetButton("Jump");
			jumpPress = Input.GetButtonDown("Jump");
			crouchHold = Input.GetButton("Crouch");
			proneHold = Input.GetButton("Prone");
			sprintHold = Input.GetButton("Sprint");
			leanLeftHold = Input.GetButton("Lean Left");
			leanRightHold = Input.GetButton("Lean Right");
			useHold = Input.GetButton("Use");
			holsterPress = Input.GetButtonDown("Holster Weapon");
			dropPress = Input.GetButtonDown("Drop Weapon");
			bulletTimePress = Input.GetButtonDown("Bullet Time");
			moveHold = Input.GetButton("Move Object");
			movePress = Input.GetButtonDown("Move Object");
			throwHold = Input.GetButton("Throw Object");
			helpPress = Input.GetButtonDown("Help");
			pausePress = Input.GetButtonDown("Pause");
			restartPress = Input.GetButtonDown("Restart");
			exitHold = Input.GetButton("Exit");
			selectNextPress = Input.GetButtonDown("Select Next Weapon");
			selectPrevPress = Input.GetButtonDown("Select Previous Weapon");
			selectWeap1Press = Input.GetButtonDown("Select Weapon 1");
			selectWeap2Press = Input.GetButtonDown("Select Weapon 2");
			selectWeap3Press = Input.GetButtonDown("Select Weapon 3");
			selectWeap4Press = Input.GetButtonDown("Select Weapon 4");
			selectWeap5Press = Input.GetButtonDown("Select Weapon 5");
			selectWeap6Press = Input.GetButtonDown("Select Weapon 6");
			selectWeap7Press = Input.GetButtonDown("Select Weapon 7");
			selectWeap8Press = Input.GetButtonDown("Select Weapon 8");
			selectWeap9Press = Input.GetButtonDown("Select Weapon 9");
			selectWeap0Press = Input.GetButtonDown("Select Weapon 0");

		}
		else
		{
			fireHold = false;//stop shooting if level is restarting
		}

	}

	//accelerate axis input for easier control and reduction of axis drift (deadzone improvement)
	float AccelerateInput(float input)
	{
		float inputAccel;
		inputAccel = ((1.0f / 4.0f) * input * (Mathf.Abs(input) * 4.0f)) * Time.smoothDeltaTime * 60.0f;
		return inputAccel;
	}

}
