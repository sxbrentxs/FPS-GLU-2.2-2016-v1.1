//LevelLoadFade.cs by Azuline Studios© All Rights Reserved
//
//Rewritten by Grafisch Lyceum student Brent Dijsselbloem.
using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LevelLoadFade : MonoBehaviour
{
	GameObject LevelLoadFadeobj;

	/// <summary>
	///		Fades the screen in from the desired <c>color</c> for <c>fadeLength</c>.
	/// </summary>
	/// <param name="color">
	///		The color you want the fade to be (Generally <c>Color.black</c>).
	/// </param>
	/// <param name="fadeLength">
	///		The time it takes to fade in.
	/// </param>
	public void FadeIn(Color color, float fadeLength)
	{
		InitiateObjectAndTexture(color);

		StartCoroutine(DoFadeIn(fadeLength));
	}
	/// <summary>
	///		Fades the screen out to the desired <c>color</c> for <c>fadeLength</c>.
	/// </summary>
	/// <param name="color">
	///		The color you want the fade to be (Generally <c>Color.black</c>).
	/// </param>
	/// <param name="fadeLength">
	///		The time it takes to fade out.
	/// </param>
	public void FadeOut(Color color, float fadeLength)
	{
		InitiateObjectAndTexture(color);

		StartCoroutine(DoFadeOut(color, fadeLength, false, null));
	}
	/// <summary>
	///		Fades the screen out to the desired <c>color</c> for <c>fadeLength</c>, then fades it back in.
	/// </summary>
	/// <param name="color">
	///		The color you want the fade to be (Generally <c>Color.black</c>).
	/// </param>
	/// <param name="fadeLength">
	///		The time it takes to fade in/out.
	/// </param>
	public void FadeInAndOut(Color color, float fadeLength)
	{
		InitiateObjectAndTexture(color);

		StartCoroutine(DoFadeOut(color, fadeLength, true, null));
	}
	/// <summary>
	///		Fades the screen to the desired <c>color</c> for <c>fadeLength</c> to the next <c>level</c>.
	/// </summary>
	/// <param name="color">
	///		The color you want the fade to be (Generally <c>Color.black</c>).
	/// </param>
	/// <param name="fadeLength">
	///		The time it takes to fade out.
	/// </param>
	/// <param name="Level">
	///		The level index as <c>System.Int32</c>.
	/// </param>
	public void FadeAndLoadLevel(Color color, float fadeLength, int level)
	{
		InitiateObjectAndTexture(color);

		StartCoroutine(DoFadeOut(color, fadeLength, false, level));
	}
	/// <summary>
	///		Fades the screen to the desired <c>color</c> for <c>fadeLength</c> to the next <c>level</c>
	/// </summary>
	/// <param name="color">
	///		The color you want the fade to be (Generally <c>Color.black</c>)
	/// </param>
	/// <param name="fadeLength">
	///		The time it takes to fade out.
	/// </param>
	/// <param name="Level">
	///		The level name as <c>System.String</c>.
	/// </param>
	public void FadeAndLoadLevel(Color color, float fadeLength, string level)
	{
		InitiateObjectAndTexture(color);

		StartCoroutine(DoFadeOut(color, fadeLength, false, level));
	}

	void InitiateObjectAndTexture(Color color)
	{
		LevelLoadFadeobj = this.gameObject;

		//Complete the fade out (Load a level or reset player position, not needed if using checkpoint spawning)
		Texture2D fadeTexture = new Texture2D(1, 1);//Create texture for screen fade
		fadeTexture.SetPixel(0, 0, color);
		fadeTexture.Apply();

		LevelLoadFadeobj.layer = LayerMask.NameToLayer("GUICamera");//set fade object's layer to one not ignored by weapon camera
		LevelLoadFadeobj.AddComponent<GUITexture>();
		LevelLoadFadeobj.transform.position = new Vector3(0.5f, 0.5f, 1000);
		LevelLoadFadeobj.GetComponent<GUITexture>().texture = fadeTexture;

		DontDestroyOnLoad(fadeTexture);
	}

	/// <summary>
	///		
	/// </summary>
	/// <param name="fadeLength">
	///		The time it takes to fade in.
	/// </param>
	/// <param name="Level">
	///		The level index as <c>int</c> or level name as <c>string</c>.
	/// </param>
	IEnumerator DoFadeIn(float fadeLength)
	{
		// Dont destroy the fade game object during level load
		DontDestroyOnLoad(LevelLoadFadeobj);

		//Create a temporary Color var and make alpha of color = 0 (transparent for starting fade out)
		Color tempColor = GetComponent<GUITexture>().color;
		tempColor.a = 0.0f;//store the color's alpha amount
		GetComponent<GUITexture>().color = tempColor;//set the guiTexture's color to the value(s) of our temporary color var

		// Fade texture in
		float time = 0.0f;
		while (time < fadeLength)
		{
			time += Time.deltaTime;
			tempColor.a = Mathf.InverseLerp(fadeLength, 0.0f, time);//smoothly fade alpha in
			GetComponent<GUITexture>().color = tempColor;
			yield return 0;
		}

		Destroy(LevelLoadFadeobj);//destroy temporary texture 

		// If we created the texture from code we used DontDestroyOnLoad,
		// which means we have to clean it up manually to avoid leaks
		Destroy(GetComponent<GUITexture>().texture);
	}
	/// <summary>
	///		
	/// </summary>
	/// <param name="fadeLength">
	///		The time it takes to fade out.
	/// </param>
	/// <param name="Level">
	///		The level index as <c>int</c> or level name as <c>string</c>.
	/// </param>
	IEnumerator DoFadeOut(Color color, float fadeLength, bool fadeBackIn, object level)
	{
		Color tempColor = GetComponent<GUITexture>().color;
		tempColor.a = 0.0f;//store the color's alpha amount
		GetComponent<GUITexture>().color = tempColor;//set the guiTexture's color to the value(s) of our temporary color var

		//Fade texture in
		float time = 0.0f;
		while (time < fadeLength)
		{
			time += Time.deltaTime;
			tempColor.a = Mathf.InverseLerp(0.0f, fadeLength, time);//smoothly fade alpha out
			GetComponent<GUITexture>().color = tempColor;
			yield return 0;
		}

		//Complete the fade out (Load a level or reset player position, not needed if using checkpoint spawning)
		if (level is int)
			SceneManager.LoadScene((level as int?).GetValueOrDefault());
		else if (level is string)
			SceneManager.LoadScene(level as string);

		yield return new WaitForSeconds(1.0f);

		Destroy(LevelLoadFadeobj);//destroy temporary texture 

		// If we created the texture from code we used DontDestroyOnLoad,
		// which means we have to clean it up manually to avoid leaks
		Destroy(GetComponent<GUITexture>().texture);
		if (fadeBackIn)
		{
			InitiateObjectAndTexture(color);
			StartCoroutine(DoFadeIn(fadeLength));
		}
	}
}